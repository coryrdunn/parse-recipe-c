//
//  DetailViewController.m
//  ParseRecipeApp
//
//  Created by Cory Dunn on 10/29/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = self.details[@"title"];
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:self.details[@"image"]]];
    self.detailImage.image = [UIImage imageWithData:data];
    
    self.IngTxtView.text = self.details[@"Ingredients"];
    self.DirTxtView.text = self.details[@"Directions"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
