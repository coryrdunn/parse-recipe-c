//
//  ViewController.m
//  ParseRecipeApp
//
//  Created by Cory Dunn on 10/29/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "ViewController.h"
#import <Parse/Parse.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    arrayOfObjects = [NSMutableArray new];
    
    [self loadData];
    
    //NSDictionary* d1 = @{@"title": @"Grilled Cheese Sandwich", @"Ingredients":@"Bread, Butter, Cheese", @"Directions":@"Preheat griddle to med-high heat. Spread butter evenly across one side of each slice of bread. Place one slice of bread (butter side down) on griddle. Place cheese on top of bread slice. Add another slice of bread on top (butter side up). Flip sandwich over when bottom slice is golden brown. Remove from heat once both slices of bread are golden brown. Chow down!", @"image":@"http://1.bp.blogspot.com/-hOisAYJTwWE/TrCPTTspz9I/AAAAAAAAM8Y/RIqSBVtAx88/s800/The%2BPerfect%2BGrilled%2BCheese%2BSandwich%2B800%2B1581.jpg"};
    
    
    //[arrayOfObjects addObject:d1];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    
    cv = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:layout];
    cv.delegate = self;
    cv.dataSource = self;
    
    UINib* nib = [UINib nibWithNibName:@"CollectionViewCell" bundle:nil];
    [cv registerNib:nib forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:cv];

}

-(void)loadData {
    NSString *urlAsString = [NSString stringWithFormat:@"http://parserecipeappc.herokuapp.com/parse/classes/recipeObject"];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"parseRecipeApp" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        retDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        for(id values in [retDict objectForKey:@"results"]) {
            NSDictionary* d = @{@"title": [values objectForKey:@"title"], @"image": [values objectForKey:@"img"], @"ing": [values objectForKey:@"ing"], @"dir": [values objectForKey:@"dir"]};
            [arrayOfObjects addObject:d];
        }
        
        [self->cv reloadData];
    }];
    [postDataTask resume];

}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayOfObjects.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary* d = arrayOfObjects[indexPath.row];
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:d[@"image"]]];
    cell.imageView.image = [UIImage imageWithData:data];
    cell.lbl.text = d[@"title"];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2-20, self.view.frame.size.width/2-20);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(60, 15, 10, 15);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"celltouched" sender:indexPath];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"celltouched"]) {
        NSIndexPath* path = (NSIndexPath*)sender;
        DetailViewController* dvc = (DetailViewController*)segue.destinationViewController;
        dvc.details = [arrayOfObjects objectAtIndex:path.row];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
