//
//  AddViewController.h
//  ParseRecipeApp
//
//  Created by Cory Dunn on 10/29/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *addTitleTxtField;
@property (strong, nonatomic) IBOutlet UITextField *addImageTxtField;
@property (strong, nonatomic) IBOutlet UITextField *addIngTxtField;
@property (strong, nonatomic) IBOutlet UITextField *addDirTxtField;

@end
