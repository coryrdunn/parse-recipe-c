//
//  CollectionViewCell.h
//  ParseRecipeApp
//
//  Created by Cory Dunn on 10/29/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *lbl;

@end
