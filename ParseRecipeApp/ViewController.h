//
//  ViewController.h
//  ParseRecipeApp
//
//  Created by Cory Dunn on 10/29/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionViewCell.h"
#import "DetailViewController.h"

@interface ViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

{
    UICollectionView* cv;
    NSMutableArray* arrayOfObjects;
    NSDictionary* retDict;
}


@end

