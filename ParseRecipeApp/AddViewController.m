//
//  AddViewController.m
//  ParseRecipeApp
//
//  Created by Cory Dunn on 10/29/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "AddViewController.h"
#import <Parse/Parse.h>

@interface AddViewController ()

@end

@implementation AddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)cancelBtnTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)saveBtnTouched:(id)sender {
    
    PFObject *recipeObject = [PFObject objectWithClassName:@"recipeObject"];
    recipeObject[@"title"] = self.addTitleTxtField.text;
    recipeObject[@"img"] = self.addImageTxtField.text;
    recipeObject[@"ing"] = self.addIngTxtField.text;
    recipeObject[@"dir"] = self.addDirTxtField.text;
    [recipeObject saveInBackground];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
